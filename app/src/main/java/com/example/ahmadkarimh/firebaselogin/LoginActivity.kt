package com.example.ahmadkarimh.firebaselogin

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {
    private var mAuth: FirebaseAuth?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        mAuth= FirebaseAuth.getInstance()

    }

    fun buLogin(view: View){
//        Toast.makeText(applicationContext, "pencet", Toast.LENGTH_SHORT).show()

        var email = etEmail.text.toString()
        var password = etPassword.text.toString()
        mAuth!!.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this){task->
                    if(task.isSuccessful){
                        Toast.makeText(applicationContext, "login success", Toast.LENGTH_SHORT).show()
                    }else{
                        Toast.makeText(applicationContext, task.exception.toString(), Toast.LENGTH_SHORT).show()
                    }
                }
    }

    override fun onStart() {
        super.onStart()
        var currentUser= mAuth!!.currentUser
        if(currentUser!=null){
            var intent = Intent(this, MainActivity::class.java)
            intent.putExtra("email", currentUser.email)
            intent.putExtra("uid", currentUser.uid)
            startActivity(intent)
    }


    }

}
